import time
import xlwings
from openpyxl import load_workbook

def main():
	while True:
		data = load_workbook('rumus.xlsx')
		tanya = input('Silahkan masukkan pesan: ')
		data['rumus']['A2'] = tanya
		data.save('rumus.xlsx')

		if(tanya=='exit'):
			break

		excel_app = xlwings.App(visible=False)
		excel_book = excel_app.books.open('rumus.xlsx')
		excel_book.save()
		excel_book.close()
		excel_app.quit()

		pesan = load_workbook('rumus.xlsx', data_only=True)
		print(pesan['rumus']['B2'].value)

		time.sleep(3)
			
if __name__ == '__main__':
	main()		