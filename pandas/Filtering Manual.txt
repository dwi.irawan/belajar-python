## FILTERING
# Filter berdasarkan Showroom
df_mbm = df_olah[(df_olah.Showroom == 'MBM')]
df_mmk = df_olah[(df_olah.Showroom == 'MMK')]
df_ksk = df_olah[(df_olah.Showroom == 'KSK')]
df_pwt = df_olah[(df_olah.Showroom == 'PWT')]
df_pbg = df_olah[(df_olah.Showroom == 'PBG')]

## GROUPING
# Berdasarkan Sales
df_group_mbm = df_mbm.groupby('Nama_Kry').sum()
df_group_mmk = df_mmk.groupby('Nama_Kry').sum()
df_group_ksk = df_ksk.groupby('Nama_Kry').sum()
df_group_pwt = df_pwt.groupby('Nama_Kry').sum()
df_group_pbg = df_pbg.groupby('Nama_Kry').sum()
