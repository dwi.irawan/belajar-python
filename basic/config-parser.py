import configparser
import ast

config = configparser.ConfigParser()
config.read('config-parser.ini')

host = config['mysql']['host']
user = config['mysql']['user']
passwd = config['mysql']['passwd']
db = config['mysql']['db']

print('MySQL configuration:')

print(f'Host: {host}')
print(f'User: {user}')
print(f'Password: {passwd}')
print(f'Database: {db}')

# Akses List
my_list = ast.literal_eval(config.get("Data", "option"))
print(my_list)

list_number = len(my_list)

for i in range(list_number):
	print(my_list[i])