import time
from time import gmtime, strftime

def main():
	while True:
		hari = strftime("%a")
		print('Hari: ' + hari)

		jam = strftime("%H:%M")
		print('Pukul: ' + jam)

		if(hari!='Sun' and jam>='08' and jam<='17'):
			# jalankan program pada hari dan jam kerja
			program()
		else:
			print('Program tidak jalan')
			# jika program tidak dijalankan berikan delay yg panjang
			# agar tidak membebankan memori
			time.sleep(10)

		now = strftime("%d/%m/%Y 00:00:00")
		print(now)

		print()
		time.sleep(1)

def program():
	print('Program dijalankan')

if __name__ == '__main__':
    main()	