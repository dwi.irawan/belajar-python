import sqlite3
conn = sqlite3.connect('example.db')

c = conn.cursor()

# Source: https://docs.python.org/3/library/sqlite3.html

# Create table
#c.execute('''CREATE TABLE stocks
#             (date text, trans text, symbol text, qty real, price real)''')

# Insert a row of data
#c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

# Save (commit) the changes
#conn.commit()

# Select Data
c.execute('SELECT * FROM stocks')
rows = c.fetchall()
for row in rows:
	print('Trans: '+row[1])

# Row count
c.execute('SELECT count(*) FROM stocks')
rows = c.fetchall()
for row in rows:
	print('Jumlah: '+str(row[0]))

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()