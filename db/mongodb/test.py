import pymongo

db = pymongo.MongoClient("mongodb://localhost:27017/")

# database
dbku = db["blog"]
# collection
collectionku = dbku['artikel']

# data yang akan diinsert
data = { "judul": "belajar ngoding python-mongodb", "penulis": "Faqih" }

# data diinsert
collectionku.insert_one(data)