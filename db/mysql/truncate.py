import mysql.connector

db = mysql.connector.connect(
	host = "localhost",
	user = "root",
	passwd = "",
	database = "toko_mainan"
)

cursor = db.cursor()
sql = "TRUNCATE customers"
cursor.execute(sql)

db.commit()

print("Data berhasil dibersihkan")